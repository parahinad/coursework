package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Floor;
import com.android.coursework.rva.FloorsForUsersRVA;
import com.android.coursework.rva.FloorsRVA;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FloorsForUsersActivity extends AppCompatActivity {

    private static final String TAG = "FloorsForUsersActivity";

    private List<Floor> mFloors;
    private RecyclerView mRecyclerView;
    private Button mBackButton;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floors_for_users);

        mRecyclerView = (RecyclerView)findViewById(R.id.floors_fu_rv);
        mBackButton = findViewById(R.id.back_btn);
        mApi = NetworkService.getInstance().getApiService();

        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        initializeData();
        initializeAdapter();

        mBackButton.setOnClickListener((v) ->  {
            finish();
        });
    }


//    private void initializeData(){ //TODO; delete
//        mFloors = new ArrayList<>();
////        mFloors.add(new Floor(0, 1, new byte[0], "", ""));
////        mFloors.add(new Floor(1, 2, new byte[0], "", ""));
////        mFloors.add(new Floor(2, 3, new byte[0], "", ""));
//    }
private void initializeData(){
    mFloors = new ArrayList<>();
    mApi.getFloors().enqueue(floorsCallback);
}

    Callback<List<Floor>> floorsCallback = new Callback<List<Floor>>() {
        @Override
        public void onResponse(Call<List<Floor>> call, Response<List<Floor>> response) {
            if(response.isSuccessful()) {
                System.out.println(response.body());
            }
            List<Floor> mFloorsList = response.body();
            for (Floor floor : mFloorsList) {
                if(floor.getBuildingId() == getIntent().getIntExtra("bId", -1))
                    mFloors.add(new Floor(floor.getId(), floor.getNumber(), floor.getPlanPhoto()));
            }
            initializeAdapter();
        }

        @Override
        public void onFailure(Call<List<Floor>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
    private void initializeAdapter(){
        FloorsForUsersRVA adapter = new FloorsForUsersRVA(this, mFloors);
        mRecyclerView.setAdapter(adapter);
    }

}
