package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.User;
import com.android.coursework.utils.Verification;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private EditText mEmailEditText, mPasswordEditText, mConfirmPasswordEditText;
    private Button mRegisterButton;
    private TextView mGoToLogin;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mEmailEditText = findViewById(R.id.reg_email_input);
        mPasswordEditText = findViewById(R.id.reg_pass_input);
        mConfirmPasswordEditText = findViewById(R.id.reg_pass_confirm_input);

        mRegisterButton = findViewById(R.id.reg_btn);
        mRegisterButton.setOnClickListener((v) ->  {
            if(Verification.verifyEmail(this, mEmailEditText) &
                    Verification.verifyPassword(this, mPasswordEditText) & Verification.verifyConfirmPassword(this, mConfirmPasswordEditText, mPasswordEditText)) {
                //navigateToMenuScreen();
                registerUser();
            }
        });

        mGoToLogin = findViewById(R.id.reg_log_btn);
        mGoToLogin.setOnClickListener((v) -> navigateToLoginScreen());
        mApi = NetworkService.getInstance().getApiService();
    }

    private void registerUser() {
        User user = User
                .getInstance()
                .setPassword(mPasswordEditText.getText().toString())
                .setEmail(mEmailEditText.getText().toString())
                .setConfirmPassword(mConfirmPasswordEditText.getText().toString());
        mApi.registerUser(user).enqueue(registerCallback);
    }

    private Callback<String> registerCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            List<String> CookieList = response.headers().values("Set-Cookie");
            if(CookieList.size() != 0) {
                getUserData(User.getInstance().getEmail());
            }else{
                Toast.makeText(RegisterActivity.this, getResources().getText(R.string.wrong_credentials), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            System.out.println(t.getMessage());
        }

    };

    private void getUserData(String sessionId) {
        mApi.getUserData(sessionId).enqueue(dataCallback);
    }

    private Callback<User> dataCallback = new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(response.body().getRole() == null) {
                User.getInstance().setId(response.body().getId()).setRole("");
            }else {
                User.getInstance().setId(response.body().getId()).setRole(response.body().getRole());
            }
            navigateToMenuScreen();
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            System.out.println(t.getMessage());
        }
    };

    private void navigateToMenuScreen() {
        Intent intent = new Intent(RegisterActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void navigateToLoginScreen() {
        Intent intent = new Intent(RegisterActivity.this,
                LoginActivity.class);
        startActivity(intent);
    }
}