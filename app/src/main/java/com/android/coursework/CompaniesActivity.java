package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.model.User;
import com.android.coursework.rva.CompaniesRVA;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompaniesActivity extends AppCompatActivity {

    private static final String TAG = "CompaniesActivity";

    private List<Company> mCompanies;
    private RecyclerView mRecyclerView;
    private Button mBackButton, mNewCompanyButton;

    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_companies);

        mApi = NetworkService.getInstance().getApiService();

        mRecyclerView = findViewById(R.id.comp_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);

        mBackButton = findViewById(R.id.back_btn);
        mBackButton.setOnClickListener((v) ->  {
            finish();
        });

        mNewCompanyButton = findViewById(R.id.add_comp_btn);
        mNewCompanyButton.setOnClickListener((v) ->  {
            navigateToNewCompanyScreen();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    private void initializeData(){
        mCompanies = new ArrayList<>();
        mApi.getCompanies(User.getInstance().getId()).enqueue(companiesCallback);
    }

    Callback<List<Company>> companiesCallback = new Callback<List<Company>>() {
        @Override
        public void onResponse(Call<List<Company>> call, Response<List<Company>> response) {
            if (response.isSuccessful()) {
                List<Company> mCompanyList = response.body();
                if (mCompanyList != null) {
                    for (Company company : mCompanyList) {
                        mCompanies.add(new Company(company.getId(), company.getName(), "1"));
                    }
                }
                initializeAdapter();
            }
        }

        @Override
        public void onFailure(Call<List<Company>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        CompaniesRVA adapter = new CompaniesRVA(this, mCompanies);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToNewCompanyScreen() {
        Intent intent = new Intent(CompaniesActivity.this,
                NewCompanyActivity.class);
        startActivity(intent);
    }
}