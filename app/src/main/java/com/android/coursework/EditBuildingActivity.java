package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.Company;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditBuildingActivity extends AppCompatActivity {

    private static final String TAG = "EditBuildingActivity";
    Button mCancelButton, mSaveButton, mEditFloorButton;
    Building mBuilding;
    TextView mTypeTextView, mAddressTextView;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_building);

        mCancelButton = findViewById(R.id.cancel_edit_build_btn);
        mSaveButton = findViewById(R.id.save_edit_build_btn);
        mEditFloorButton = findViewById(R.id.edit_floors_btn);
        mTypeTextView = findViewById(R.id.edit_type_input);
        mAddressTextView = findViewById(R.id.edit_address_input);
        mApi = NetworkService.getInstance().getApiService();

        getBuilding();
        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveBuilding();
        });

        mEditFloorButton.setOnClickListener(v -> {
            Intent intent = new Intent(EditBuildingActivity.this,
                    FloorsActivity.class);
            intent.putExtra("bId", getIntent().getIntExtra("bId", -1));
            startActivity(intent);
        });
    }

    private void saveBuilding(){
        //TODO; save to db
        //TODO; set id
        mBuilding.setType(mTypeTextView.getText().toString());
        mBuilding.setAddress(mAddressTextView.getText().toString());
        mApi.updateBuilding(getIntent().getIntExtra("bId", -1), mBuilding).enqueue(editBuildingCallback);
        finish();
    }
    Callback<Building> editBuildingCallback = new Callback<Building>() {
        @Override
        public void onResponse(Call<Building> call, Response<Building> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Building> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void getBuilding(){
        mApi.getBuilding(getIntent().getIntExtra("bId", -1)).enqueue(buildingCallBack);
    }

    Callback<Building> buildingCallBack = new Callback<Building>() {
        @Override
        public void onResponse(Call<Building> call, Response<Building> response) {
            if (response.isSuccessful()) {
                mTypeTextView.setText(response.body().getType());
                mAddressTextView.setText(response.body().getAddress());
                mBuilding = new Building(response.body().getId(), response.body().getCompanyId(), response.body().getType(), response.body().getAddress());
            }
        }

        @Override
        public void onFailure(Call<Building> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}