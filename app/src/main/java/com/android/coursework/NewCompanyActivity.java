package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewCompanyActivity extends AppCompatActivity {

    private static final String TAG = "NewCompanyActivity";

    Button mCancelButton, mSaveButton;
    EditText mCompanyName;

    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_company);

        mApi = NetworkService.getInstance().getApiService();

        mCompanyName = findViewById(R.id.name_input);

        mCancelButton = findViewById(R.id.cancel_add_comp_btn);
        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton = findViewById(R.id.save_add_comp_btn);
        mSaveButton.setOnClickListener(v -> {
            saveCompany();
        });
    }

    private void saveCompany(){
        Company company = new Company(mCompanyName.getText().toString(), User.getInstance().getId());
        System.out.println(User.getInstance().getId());
        mApi.createCompany(User.cookies, company).enqueue(createCompanyCallback);
    }

    Callback<Company> createCompanyCallback = new Callback<Company>() {
        @Override
        public void onResponse(Call<Company> call, Response<Company> response) {
            System.out.println("companies: " + response.code());
            if(response.isSuccessful()) {
                finish();
            }
        }

        @Override
        public void onFailure(Call<Company> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}