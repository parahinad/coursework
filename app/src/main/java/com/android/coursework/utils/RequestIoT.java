package com.android.coursework.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import com.android.coursework.CurrentPlanActivity;
import com.android.coursework.R;
import com.android.coursework.model.User;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;

import static androidx.core.content.ContextCompat.startActivity;


public class RequestIoT extends AsyncTask<Void,Void,String> {

    String ip;
    String userId;
    Activity mActivity;
    Context mContext, menuContext;
    Bundle mBundle;
    int TIMEOUT_VALUE = 1000;

    public RequestIoT(Activity activity, Context context, Bundle bundle, Context menuContext){
        this.mActivity = activity;
        this.mContext = context;
        this.mBundle = bundle;
        this.menuContext = menuContext;
    }

    public void setIP(String ip) {
        this.ip = ip;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            URL url = new URL("http://" + ip + "/id");
            URLConnection httpURLConnection = url.openConnection();
            String urlParameters = "user_id=" + userId;
            httpURLConnection.setDoOutput(true);

            httpURLConnection.setConnectTimeout(TIMEOUT_VALUE);
            httpURLConnection.setReadTimeout(TIMEOUT_VALUE);

            OutputStreamWriter writer = new OutputStreamWriter(httpURLConnection.getOutputStream());

            writer.write(urlParameters);
            writer.flush();

            String line;
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));

            while ((line = reader.readLine()) != null) {
                User.getInstance().setCurrentFloorId(Integer.parseInt(line));
                System.out.println("Floor id: " + User.getInstance().getCurrentFloorId());
            }
            writer.close();
            reader.close();
            Intent intent = new Intent(mActivity,
                CurrentPlanActivity.class);
            startActivity(mContext, intent, mBundle);
        }
        catch (MalformedURLException ignored)
        {
        } catch (SocketTimeoutException e) {
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(menuContext, R.string.ip_error, Toast.LENGTH_LONG).show();
                }
            });
        } catch (IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    protected void onPostExecute(String aVoid) {
        super.onPostExecute(aVoid);
    }
}

