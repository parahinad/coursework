package com.android.coursework.utils;

import android.content.Context;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.EditText;

import com.android.coursework.R;

import java.util.regex.Pattern;

public class Verification {

    public static boolean verifyName(Context context, EditText input) {
        return true;
    }

    public static boolean verifyEmail(Context context, EditText input) {
        String email = input.getText().toString();
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        if (pattern.matcher(email).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.email_tip));
            return false;
        }
    }

    public static boolean verifyPassword(Context context,EditText input) {
        String password = input.getText().toString();
        Pattern PASSWORD_PATTERN = Pattern
                .compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*.!@$%^&(){}:;<>,?/~_+-=|]).{8,32}$");
        if (!TextUtils.isEmpty(password) && PASSWORD_PATTERN.matcher(password).matches()) {
            return true;
        } else {
            input.setError(context.getString(R.string.password_tip));
            return false;
        }
    }

    public static boolean verifyConfirmPassword(Context context, EditText input, EditText password) {
        String confirmPassword = input.getText().toString();
        if (confirmPassword.equals(password.getText().toString())) {
            return true;
        } else {
            input.setError(context.getString(R.string.confirm_pass_error));
            return false;
        }
    }

    public static boolean verifyIP(Context context, EditText input) {
        String ip = input.getText().toString();
        return true;
        //Pattern IP_PATTERN = Pattern.compile("^(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)(\\.(25[0-5]|2[0-4]\\d|[0-1]?\\d?\\d)){3}$");
//        if (!TextUtils.isEmpty(ip) && IP_PATTERN.matcher(ip).matches()) {
//            return true;
//        } else {
//            input.setError(context.getString(R.string.ip_tip));
//            return false;
//        }
    }
}
