package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.User;
import com.android.coursework.rva.BuildingsRVA;
import com.android.coursework.rva.VisitedBuildingsRVA;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitedBuildingsActivity extends AppCompatActivity {

    private static final String TAG = "VisitedBuildingsActivity";

    private List<Building> mBuildings;
    private RecyclerView rv;
    private Button mBackButton;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visited_buildings);

        rv = (RecyclerView)findViewById(R.id.visited_build_rv);
        mBackButton = findViewById(R.id.back_btn);
        LinearLayoutManager llm = new LinearLayoutManager(this);

        mApi = NetworkService.getInstance().getApiService();

        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);

        initializeData();

        mBackButton.setOnClickListener((v) ->  {
            finish();
        });
    }

    private void initializeData(){
        mBuildings = new ArrayList<>();
        mApi.getVisitedBuilding(User.getInstance().getId()).enqueue(buildingsCallback);
    }

    Callback<List<Building>> buildingsCallback = new Callback<List<Building>>() {
        @Override
        public void onResponse(Call<List<Building>> call, Response<List<Building>> response) {
            if(response.isSuccessful()) {
                List<Building> mBuildingsList = response.body();
                for (Building building : mBuildingsList) {
                    mBuildings.add(new Building(building.getId(), building.getCompanyId(), building.getType(), building.getAddress()));
                }
                initializeAdapter();
            }
        }

        @SuppressLint("LongLogTag")
        @Override
        public void onFailure(Call<List<Building>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        VisitedBuildingsRVA adapter = new VisitedBuildingsRVA(this, mBuildings);
        rv.setAdapter(adapter);
    }

    private void navigateToMenuScreen() {
        Intent intent = new Intent(VisitedBuildingsActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void navigateToSearchScreen() {
        Intent intent = new Intent(VisitedBuildingsActivity.this,
                SearchActivity.class);
        startActivity(intent);
    }
}