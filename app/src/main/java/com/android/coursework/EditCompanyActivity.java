package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditCompanyActivity extends AppCompatActivity {

    private static final String TAG = "EditCompanyActivity";

    Button mCancelButton, mSaveButton, mEditBuildButton;
    TextView mCompanyNameTextView;
    private JSONPlaceHolderApi mApi;
    Company mCompany;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_company);

        mCancelButton = findViewById(R.id.cancel_edit_comp_btn);
        mSaveButton = findViewById(R.id.save_edit_comp_btn);
        mEditBuildButton = findViewById(R.id.edit_build_btn);
        mCompanyNameTextView = findViewById(R.id.edit_name_input);
        mApi = NetworkService.getInstance().getApiService();

        getCompany();
        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveCompany();
        });

        mEditBuildButton.setOnClickListener(v -> {
            Intent intent = new Intent(EditCompanyActivity.this,
                    BuildingsActivity.class);
            intent.putExtra("cId", getIntent().getIntExtra("cId", -1));
            startActivity(intent);
        });
    }

    private void saveCompany() {
        mCompany.setName(mCompanyNameTextView.getText().toString());
        mApi.updateCompany(getIntent().getIntExtra("cId", -1), mCompany).enqueue(editCompanyCallback);
    }

    Callback<Company> editCompanyCallback = new Callback<Company>() {
        @Override
        public void onResponse(Call<Company> call, Response<Company> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                finish();
            }
        }

        @Override
        public void onFailure(Call<Company> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void getCompany() {
        mApi.getCompanies(getIntent().getIntExtra("cId", -1)).enqueue(companyCallback);
    }

    Callback<Company> companyCallback = new Callback<Company>() {
        @Override
        public void onResponse(Call<Company> call, Response<Company> response) {
            if (response.isSuccessful()) {
                mCompanyNameTextView.setText(response.body().getName());
                mCompany = new Company(response.body().getId(), response.body().getName(), User.getInstance().getId());
            }
        }

        @Override
        public void onFailure(Call<Company> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}