package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Floor;
import com.android.coursework.model.Company;
import com.android.coursework.model.Floor;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditFloorActivity extends AppCompatActivity {

    private static final String TAG = "EditFloorActivity";
    private static final int RESULT_LOAD_IMG = 1;

    Button mCancelButton, mSaveButton, mEditPlanButton;
    private JSONPlaceHolderApi mApi;
    Floor mFloor;
    String floorPlan;
    TextView mStatusTextView;
    EditText mFloorNumberEditText, mIPEditText, mCodeEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_floor);
        mCancelButton = findViewById(R.id.cancel_edit_floor_btn);
        mSaveButton = findViewById(R.id.save_edit_floor_btn);
        mEditPlanButton = findViewById(R.id.edit_plan_btn);
        mStatusTextView = findViewById(R.id.plan_status);
        mFloorNumberEditText = findViewById(R.id.edit_floor_input);
        mIPEditText = findViewById(R.id.edit_floor_ip_input);
        mCodeEditText = findViewById(R.id.edit_floor_code_input);

        mApi = NetworkService.getInstance().getApiService();

        getFloor();
        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveFloor();
        });

        mEditPlanButton.setOnClickListener(v -> {
            editPlan();
        });

    }

    private void saveFloor() {
        //TODO; save to db
        //TODO; set id
        //finish();
        //TODO add new floor plan to mFloor as in NewFloorActivity;
        mFloor.setNumber(Integer.parseInt(mFloorNumberEditText.getText().toString()));
        mFloor.setIpAddress(mIPEditText.getText().toString());
        mFloor.setAccessCode(mCodeEditText.getText().toString());
        if(floorPlan != null)
            mFloor.setPlanPhoto(floorPlan);
        mApi.updateFloor(getIntent().getIntExtra("fId", -1), mFloor).enqueue(editFloorCallback);
    }

    Callback<Floor> editFloorCallback = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            System.out.println(response.code());
            if (response.isSuccessful()) {

                finish();
            }
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void getFloor() {
        mApi.getFloor(getIntent().getIntExtra("fId", -1)).enqueue(floorCallBack);
    }

    Callback<Floor> floorCallBack = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            if (response.isSuccessful()) {
                mFloorNumberEditText.setText(Integer.toString(response.body().getNumber()));
                mIPEditText.setText(response.body().getIpAddress());
                mCodeEditText.setText(response.body().getAccessCode());

                mFloor = new Floor(response.body().getId(),
                        response.body().getBuildingId(),
                        response.body().getNumber(),
                        response.body().getIpAddress(),
                        response.body().getAccessCode(),
                        response.body().getPlanPhoto());
            }
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void editPlan() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                String imgString = Base64.encodeToString(getBytesFromBitmap(selectedImage),
                        Base64.NO_WRAP);
                floorPlan = imgString;
                String path = getPath(EditFloorActivity.this, imageUri);

                mStatusTextView.setText(path);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(EditFloorActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(EditFloorActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }
}