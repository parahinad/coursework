package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.Floor;
import com.android.coursework.model.User;
import com.android.coursework.utils.TouchImageView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FloorPlanActivity extends AppCompatActivity {

    Button mBackButton;
    private JSONPlaceHolderApi mApi;
    Building mBuilding;
    TouchImageView mPlanImageView;
    TextView mFloorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floor_plan);
        mPlanImageView = findViewById(R.id.plan_img);
        mFloorTextView = findViewById(R.id.plan_label);
        mApi = NetworkService.getInstance().getApiService();

        getPlan();
        mBackButton = findViewById(R.id.back_btn);

        mBackButton.setOnClickListener((v) ->  {
            finish();
        });
    }


    private void getPlan() {
        mApi.getFloor(getIntent().getIntExtra("fId", -1)).enqueue(getPlanCallback);
    }

    private Callback<Floor> getPlanCallback = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            if(response.isSuccessful()){
                String label = response.body().getNumber() + " " + getResources().getString(R.string.floor);
                mFloorTextView.setText(label);
                mPlanImageView.setImageBitmap(StringToBitMap(response.body().getPlanPhoto()));
            }
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            System.out.println(t.getMessage());
        }
    };

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }
        catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}