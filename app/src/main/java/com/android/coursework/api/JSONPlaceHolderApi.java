package com.android.coursework.api;

import com.android.coursework.model.Building;
import com.android.coursework.model.Company;
import com.android.coursework.model.Floor;
import com.android.coursework.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface JSONPlaceHolderApi {

    @GET("/api/companies")
    Call<List<Company>> getCompanies(@Query("userId") String userId);

    @GET("/api/companies/{id}")
    Call<Company> getCompanies(@Path("id") int id);

    @POST("/api/companies")
    Call<Company> createCompany(@Header("Cookie") String sessionIdAndToken, @Body Company company);

    @PUT("/api/companies/{id}")
    Call<Company> updateCompany(@Path("id") int id, @Body Company company);

    @DELETE("/api/companies/{id}")
    Call<Company> deleteCompany(@Path("id") long id);

    @GET("/api/buildings")
    Call <List<Building>> getBuildings();

    @GET("/api/buildings/{id}")
    Call<Building> getBuilding(@Path("id") int id);

    @POST("/api/buildings")
    Call<Building> createBuilding(@Body Building building); //TODO needs "can manage companies"

    @PUT("/api/buildings/{id}")
    Call<Building> updateBuilding(@Path("id") int id, @Body Building building);

    @DELETE("/api/buildings/{id}")
    Call<Building> deleteBuilding(@Path("id") int id);

    @GET("/api/floors")
    Call <List<Floor>> getFloors();

    @GET("/api/floors/{id}")
    Call<Floor> getFloor(@Path("id") int id);

    @POST("/api/floors")
    Call<Floor> createFloor(@Body Floor floor);

    @PUT("/api/floors/{id}")
    Call<Floor> updateFloor(@Path("id") int id, @Body Floor floor);

    @DELETE("/api/floors/{id}")
    Call<Floor> deleteFloor(@Path("id") int id);

    @POST("/Account/Login")
    Call<String> loginUser(@Body User user);

    @POST("/Account/Register")
    Call<String> registerUser(@Body User user);

    @GET("/api/user")
    Call<User> getUserData(@Query("email") String email);

    @GET("/api/address")
    Call<Floor> getIpAddress(@Query("code") int code);

    @GET("/api/buildings")
    Call<Building> getFloorBuilding(@Query("floorId") int floorId);

    @POST("/api/buildings")
    Call<Building> setCurrentBuilding(@Query("userId") String userId, @Query("buildingId") int buildingId, @Body Building building);

    @GET("/api/buildings")
    Call<List<Building>> getVisitedBuilding(@Query("userId") String userId);

}
