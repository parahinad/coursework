package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.Floor;
import com.android.coursework.model.User;
import com.android.coursework.rva.FloorsRVA;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FloorsActivity extends AppCompatActivity {

    private static final String TAG = "FloorsActivity";

    private List<Floor> mFloors;
    private RecyclerView mRecyclerView;
    private Button mBackButton, mNewFloorButton;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floors);

        mRecyclerView = (RecyclerView)findViewById(R.id.floor_rv);
        mBackButton = findViewById(R.id.back_btn);
        mNewFloorButton = findViewById(R.id.add_floor_btn);

        mApi = NetworkService.getInstance().getApiService();

        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        mBackButton.setOnClickListener((v) ->  {
            navigateToMenuScreen();
            finish();
        });

        mNewFloorButton.setOnClickListener((v) ->  {
            navigateToNewFloorScreen();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    private void initializeData(){
        mFloors = new ArrayList<>();
        mApi.getFloors().enqueue(floorsCallback);
    }

    Callback<List<Floor>> floorsCallback = new Callback<List<Floor>>() {
        @Override
        public void onResponse(Call<List<Floor>> call, Response<List<Floor>> response) {
            if(response.isSuccessful()) {
                System.out.println(response.body());
            }
            List<Floor> mFloorsList = response.body();
            for (Floor floor : mFloorsList) {
                if(floor.getBuildingId() == getIntent().getIntExtra("bId", -1))
                    mFloors.add(new Floor(floor.getId(), floor.getNumber(), floor.getPlanPhoto()));
            }
            initializeAdapter();
        }

        @Override
        public void onFailure(Call<List<Floor>> call, Throwable t) {
            System.out.println(t);
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        FloorsRVA adapter = new FloorsRVA(this, mFloors);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToMenuScreen() {
        Intent intent = new Intent(FloorsActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void navigateToNewFloorScreen() {
        Intent intent = new Intent(FloorsActivity.this,
                NewFloorActivity.class);
        intent.putExtra("bId", getIntent().getIntExtra("bId", -1));
        startActivity(intent);
    }

}