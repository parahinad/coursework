package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.coursework.model.User;

public class MenuActivity extends AppCompatActivity {

    private TextView mLogOut;
    private Button mCurrentPlan;
    private Button mVisitedBuildings;
    private Button mManageCompanies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        showButtons();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (User.getInstance().getCurrentFloorId() == -1) {
            mCurrentPlan.setText(R.string.enter_the_building);
        }else {
            mCurrentPlan.setText(R.string.current_plan);
        }
    }

    private void showButtons() {
        mLogOut = findViewById(R.id.log_out);
        mLogOut.setOnClickListener(v -> {
            //TODO; logout user
            Intent intent = new Intent(MenuActivity.this, LoginActivity.class);
            startActivity(intent);
            //finish();
        });

        mCurrentPlan = findViewById(R.id.current_plan_button);
        mCurrentPlan.setText(R.string.enter_the_building);
        mCurrentPlan.setOnClickListener(v -> {
            if(User.getInstance().getCurrentFloorId() == -1) {
                FragmentManager manager = getSupportFragmentManager();
                IPDialogFragment ipDialogFragment = new IPDialogFragment(this);
                ipDialogFragment.show(manager, "myDialog");
            }else {
                Intent intent = new Intent(MenuActivity.this,
                        CurrentPlanActivity.class);
                startActivity(intent);
            }
        });

        mVisitedBuildings = findViewById(R.id.visited_buildings_button);
        mVisitedBuildings.setOnClickListener(v -> {
            Intent intent = new Intent(MenuActivity.this,
                    VisitedBuildingsActivity.class);
            startActivity(intent);
        });

        mManageCompanies = findViewById(R.id.manage_companies_button);
        if (!User.getInstance().getRole().equals("CanManageCompanies")) {
            mManageCompanies.setVisibility(View.GONE);
        } else {
            mManageCompanies.setVisibility(View.VISIBLE);
            mManageCompanies.setOnClickListener(v -> {
                Intent intent = new Intent(MenuActivity.this,
                        CompaniesActivity.class);
                startActivity(intent);
            });
        }
    }



}
