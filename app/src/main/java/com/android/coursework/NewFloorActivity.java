package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Floor;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewFloorActivity extends AppCompatActivity {

    private static final String TAG = "NewFloorActivity";
    private static final int RESULT_LOAD_IMG = 1;

    Button mCancelButton, mSaveButton, mAddPlanButton;
    private JSONPlaceHolderApi mApi;
    EditText mFloorNumberEditText, mIPEditText, mCodeEditText;
    String floorPlan;
    TextView mStatusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_floor);

        mCancelButton = findViewById(R.id.cancel_add_floor_btn);
        mSaveButton = findViewById(R.id.save_add_floor_btn);
        mAddPlanButton = findViewById(R.id.add_plan_btn);
        mFloorNumberEditText = findViewById(R.id.floor_input);
        mIPEditText = findViewById(R.id.floor_ip_input);
        mCodeEditText = findViewById(R.id.floor_code_input);
        mStatusTextView = findViewById(R.id.plan_status);

        mApi = NetworkService.getInstance().getApiService();

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveFloor();
        });

        mAddPlanButton.setOnClickListener(v -> {
            addFloorPlan();
        });

    }

    private void saveFloor() {
        //TODO; save to db
        //TODO; set id
        Floor floor = new Floor();
        floor.setBuildingId(getIntent().getIntExtra("bId", -1));
        floor.setNumber(Integer.parseInt(mFloorNumberEditText.getText().toString()));
        floor.setIpAddress(mIPEditText.getText().toString());
        floor.setAccessCode(mCodeEditText.getText().toString());

        if(floorPlan != null)
            floor.setPlanPhoto(floorPlan);
        mApi.createFloor(floor).enqueue(createFloorCallback);
    }

    Callback<Floor> createFloorCallback = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            if (response.isSuccessful()) {
                finish();
            }
            finish();
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void addFloorPlan() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, RESULT_LOAD_IMG);
    }

    @Override
    protected void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);


        if (resultCode == RESULT_OK) {
            try {
                final Uri imageUri = data.getData();
                final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);

                String imgString = Base64.encodeToString(getBytesFromBitmap(selectedImage),
                        Base64.NO_WRAP);
                floorPlan = imgString;
                String path = getPath(NewFloorActivity.this, imageUri);

                mStatusTextView.setText(path);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Toast.makeText(NewFloorActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(NewFloorActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    public byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }
}