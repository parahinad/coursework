package com.android.coursework.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Building {

    @SerializedName("Id")
    @Expose
    private int id;

//    @SerializedName("name")
//    @Expose
//    private String name;
//
//    @SerializedName("numoffloors")
//    @Expose
//    private String countFloor;

    @SerializedName("CompanyId")
    @Expose
    private int companyId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @SerializedName("userId")
    @Expose
    private String userId;

    @SerializedName("Address")
    @Expose
    private String address;

    @SerializedName("Type")
    @Expose
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public Building(int id, int companyId, String type, String address) {
        this.id = id;
        this.companyId = companyId;
        this.type = type;
        this.address = address;
    }
    public Building(){}

    public Building(int id, String address, String type) {
        this.id = id;
        this.address = address;
        this.type = type;
    }
}