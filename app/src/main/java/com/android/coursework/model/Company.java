package com.android.coursework.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Company {

    @SerializedName("Id")
    @Expose
    private int id;

    @SerializedName("Name")
    @Expose
    private String name;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @SerializedName("ApplicationUserId")
    @Expose
    private String userId;

//    @SerializedName("ApplicationUserId")
//    @Expose
//    private String userID;
//
    @SerializedName("numofbuild")  //TODO
    @Expose
    private String countBuild;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
//
//    public String getUserID() {
//        return userID;
//    }
//
//    public void setUserID(String userID) {
//        this.userID = userID;
//    }
//
//    public String getCountBuild() {
//        return countBuild;
//    }
//
//    public void setCountBuild(String countBuild) {
//        this.countBuild = countBuild;
//    }

    public Company(int id, String name, String userID) {
        this.id = id;
        this.name = name;
        this.userId = userID;
    }
    public Company(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Company(String name, String userID) {
        this.name = name;
        this.userId = userID;
    }

}