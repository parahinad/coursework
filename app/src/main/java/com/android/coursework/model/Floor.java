package com.android.coursework.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Floor {

    @SerializedName("Id")
    @Expose
    private int id;

    @SerializedName("Number")
    @Expose
    private int number;

//    @SerializedName("EvacuationPlan")  //TODO
//    @Expose
//    private byte[] planPhoto;
    @SerializedName("EvacuationPlan")  //TODO
    @Expose
    private String planPhoto;

    @SerializedName("IpAddress")
    @Expose
    private String ipAddress;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @SerializedName("ip")
    @Expose
    private String ip;

    @SerializedName("AccessCode")
    @Expose
    private String accessCode;

    @SerializedName("BuildingId")
    @Expose
    private int buildingId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPlanPhoto() {
        return planPhoto;
    }

    public void setPlanPhoto(String planPhoto) {
        this.planPhoto = planPhoto;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    public Floor(int id, int buildingId, int number, String ipAddress, String accessCode, String planPhoto) {
        this.id = id;
        this.buildingId = buildingId;
        this.number = number;
        this.ipAddress = ipAddress;
        this.accessCode = accessCode;
        this.planPhoto = planPhoto;
    }
    public Floor(int id, int number, String planPhoto) {
        this.id = id;
        this.number = number;
        this.planPhoto = planPhoto;
    }
    public Floor() {}

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

//    public void setPlanPhoto(String planPhotoStr){
//        planPhoto = planPhotoStr.getBytes();
//    }
}