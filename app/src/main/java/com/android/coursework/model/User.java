package com.android.coursework.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class User {

    @SerializedName("Id")
    @Expose
    private String mId;
//    @SerializedName("name")
//    @Expose
//    private String mName;
    @SerializedName("Email")
    @Expose
    private String mEmail;
    @SerializedName("Password")
    @Expose
    private String mPassword;
    @SerializedName("ConfirmPassword")
    @Expose
    private String mConfirmPassword;
    @SerializedName("Remember me?")
    @Expose
    private String rememberMe = "false";
    @SerializedName("Role")
    @Expose
    private String Role = "";

    private static User mInstance;


    public static String cookies = ".AspNet.ApplicationCookie=Y8hHh0oBkp4Vp3nHeAfGKti_la0ShhKqXkOy55ymgj1shty3exq3o_ZhnOm-tO99yHzIoh1sCbYPopfH4FWOZdiOde3b9Sk4wRKluUXA_kQenWjVxYsayma12IUqKgkXJtm3lqzp6F-Nfh69kZNJ_uw5ZJ-EZ56ysCPFFDPf1QS5BA7nxUlnO-_wesH7TLEI1RRZRMlstq3vHjroq8nRELEI69M8yqJrvypGqsgTPmRGwze5jxx-ErXd9hKrOTixdWbokHur4yUVItKIzqUlemJL0tdZBNXoMv284TpNeBmz42-hepolsI1PIUpIZt4vOasHw6LCnAQxHRXddZ6R2mOMP2VSx7JY5dz48w3t7ce4WTnnSP9BIVdLDtwMANPTMg1LIGkcrNcJ95K88csYpFtvIpnEAPBP4nOIdkyYzqnweKdl_GK0dZ6oWE5c6EEuiwMWAYNerFJScRqTKophgdrADN-qw8uWMj439RetDoKiDjzm0Y_nwUDZnrzOdgJoVDqFWZ6WwfmEleVYPYVxQA; ASP.NET_SessionId=emqohbzvkav0vg00c3y2vxbh; __RequestVerificationToken=KSHblonMEfmIDN6n2LSU1YNeom0_PcxnMdXTvB9RpM_Ga5808KKwO4S3aV_WLPivylvDiZcL8zJrMsb1u57IRDHv7t-qyCluZAjdGWGDfgA1";    private User() {}

    public static User getInstance() {
        if(mInstance == null) {
            mInstance = new User();
        }
        return mInstance;
    }

    private int currentFloorId = -1;


    public int getCurrentFloorId() {
        return currentFloorId;
    }

    public void setCurrentFloorId(int currentFloorId) {
        this.currentFloorId = currentFloorId;
    }

    public String getId() {
        return mId;
    }

    public User setId(String id) {
        mId = id;
        return mInstance;
    }

    public String getEmail() {
        return mEmail;
    }

    public User setEmail(String email) {
        mEmail = email;
        return mInstance;
    }

    public String getPassword() {
        return mPassword;
    }

    public User setPassword(String password) {
        mPassword = password;
        return mInstance;
    }

    public String getConfirmPassword() {
        return mConfirmPassword;
    }

    public User setConfirmPassword(String confirmPassword) {
        mConfirmPassword = confirmPassword;
        return mInstance;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }


}
