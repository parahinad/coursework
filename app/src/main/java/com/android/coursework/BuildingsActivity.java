package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.Company;
import com.android.coursework.model.User;
import com.android.coursework.rva.BuildingsRVA;
import com.android.coursework.rva.CompaniesRVA;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuildingsActivity extends AppCompatActivity {

    private static final String TAG = "BuildingsActivity";

    private List<Building> mBuildings;
    private RecyclerView mRecyclerView;
    private Button mBackButton, mNewBuildingButton;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buildings);
        mRecyclerView = (RecyclerView)findViewById(R.id.build_rv);
        mBackButton = findViewById(R.id.back_btn);
        mNewBuildingButton = findViewById(R.id.add_build_btn);

        mApi = NetworkService.getInstance().getApiService();

        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);

        mBackButton.setOnClickListener((v) ->  {
            navigateToMenuScreen();
            finish();
        });

        mNewBuildingButton.setOnClickListener((v) ->  {
            navigateToNewBuildingScreen();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeData();
    }

    private void initializeData(){
        mBuildings = new ArrayList<>();
        mApi.getBuildings().enqueue(buildingsCallback);
    }

    Callback<List<Building>> buildingsCallback = new Callback<List<Building>>() {
        @Override
        public void onResponse(Call<List<Building>> call, Response<List<Building>> response) {
            if(response.isSuccessful()) {
                List<Building> mBuildingsList = response.body();
                for (Building building : mBuildingsList) {
                    if (building.getCompanyId() == getIntent().getIntExtra("cId", -1))
                       mBuildings.add(new Building(building.getId(), building.getCompanyId(), building.getType(), building.getAddress()));
                }
                initializeAdapter();
            }
        }

        @Override
        public void onFailure(Call<List<Building>> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };

    private void initializeAdapter(){
        BuildingsRVA adapter = new BuildingsRVA(this, mBuildings);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToMenuScreen() {
        Intent intent = new Intent(BuildingsActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void navigateToNewBuildingScreen() {
        Intent intent = new Intent(BuildingsActivity.this,
                NewBuildingActivity.class);
        intent.putExtra("cId", getIntent().getIntExtra("cId", -1));
        startActivity(intent);
    }
}