package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.android.coursework.model.Building;
import com.android.coursework.rva.BuildingsRVA;
import com.android.coursework.rva.SearchRVA;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private List<Building> mBuildings;
    private RecyclerView mRecyclerView;
    private Button mBackButton, mSearchButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mRecyclerView = (RecyclerView)findViewById(R.id.search_rv);
        mBackButton = findViewById(R.id.back_btn);
        mSearchButton = findViewById(R.id.search_btn);
        LinearLayoutManager llm = new LinearLayoutManager(this);

        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setHasFixedSize(true);


        mBackButton.setOnClickListener((v) ->  {
//            navigateToMenuScreen();
            finish();
        });

        mSearchButton.setOnClickListener((v) ->  {
            searchResult();
        });
    }
    private void initializeData(){ //TODO; delete
        mBuildings = new ArrayList<>();
        mBuildings.add(new Building(0, "Kharkiv, Sumska 21", "Office"));
        mBuildings.add(new Building(1, "Kharkiv, Sumska 24", "Office"));
        mBuildings.add(new Building(2,  "Kharkiv, Sumska 41", "Office"));
    }
    private void initializeAdapter(){
        SearchRVA adapter = new SearchRVA(this, mBuildings);
        mRecyclerView.setAdapter(adapter);
    }

    private void navigateToMenuScreen() {
        Intent intent = new Intent(SearchActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void searchResult(){
        initializeData();
        initializeAdapter();
    }

}