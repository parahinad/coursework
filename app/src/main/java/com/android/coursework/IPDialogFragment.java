package com.android.coursework;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Floor;
import com.android.coursework.model.User;
import com.android.coursework.utils.RequestIoT;
import com.android.coursework.utils.Verification;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class IPDialogFragment extends DialogFragment {

    Button mSubmitButton, mCurrentPlan;;
    EditText mIPEditText;
    Context menuContext;
    private JSONPlaceHolderApi mApi;
    private Bundle mSavedInstanceState;
    private Activity mActivity;
    private Context mContext;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = Objects.requireNonNull(getActivity()).getLayoutInflater();
        View view = inflater.inflate(R.layout.connection_dialog_window, null);
        builder.setView(view);
        mSubmitButton = view.findViewById(R.id.submit_code_btn);
        mCurrentPlan = view.findViewById(R.id.current_plan_button);
        mIPEditText = view.findViewById(R.id.code_input);
        mActivity = getActivity();
        mContext = getContext();
        mSubmitButton.setOnClickListener(v -> {
            if(Verification.verifyIP(getContext(), mIPEditText)) {
                mSavedInstanceState = savedInstanceState;
                getIp(Integer.parseInt(mIPEditText.getText().toString()));
                //sendRequest("192.168.31.144", savedInstanceState);
                dismiss();
            }
        });
        mApi = NetworkService.getInstance().getApiService();

        return builder.create();
    }

    private void getIp(int code){
        mApi.getIpAddress(code).enqueue(ipCallback);
    }
    private Callback<Floor> ipCallback = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            System.out.println("WOOOOW! IT WORKS!");
            System.out.println(response.body().getIp());
            if(!response.body().getIp().replaceAll("\t", "").equals(null)) {
                sendRequest(response.body().getIp().replaceAll("\t", ""), mSavedInstanceState);
            }
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            System.out.println("OOOOOOOOOUUUUUUUUPS!"); //TODO
            System.out.println(t.getMessage());
        }
    };

    private void sendRequest(String ip, Bundle bundle){
        RequestIoT requestIoT = new RequestIoT(mActivity, mContext, bundle, menuContext);
        requestIoT.setIP(ip);
        requestIoT.setUserId(User.getInstance().getId());
        requestIoT.execute();
    }

    public IPDialogFragment(Context context){
        this.menuContext = context;
    }

}


