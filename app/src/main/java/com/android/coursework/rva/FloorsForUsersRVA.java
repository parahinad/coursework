package com.android.coursework.rva;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.CompaniesActivity;
import com.android.coursework.CurrentPlanActivity;
import com.android.coursework.EditCompanyActivity;
import com.android.coursework.FloorPlanActivity;
import com.android.coursework.NewCompanyActivity;
import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.R;
import com.android.coursework.model.Floor;

import java.util.List;

public class FloorsForUsersRVA extends RecyclerView.Adapter<FloorsForUsersRVA.FloorsViewHolder> {
    private Context mContext;

    public FloorsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.floor_fu_card, viewGroup, false);

        FloorsViewHolder floorsViewHolder = new FloorsViewHolder(v);
        return floorsViewHolder;
    }

    private List<Floor> mFloors;

    public FloorsForUsersRVA(Context context, List<Floor> floors) {
        this.mContext = context;
        this.mFloors = floors;
    }

    @Override
    public void onBindViewHolder(@NonNull FloorsViewHolder holder, int position) {
        holder.mCardView.setId(mFloors.get(position).getId());
        String label = mFloors.get(position).getNumber() + " " + mContext.getResources().getString(R.string.floor);
        holder.mNameTV.setText(label);
        holder.mPlanPhoto.setImageBitmap(StringToBitMap(mFloors.get(position).getPlanPhoto()));

        holder.mCardView.setOnClickListener(v -> {
           openPlan(holder.mCardView.getId());
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mFloors.size();
    }

    class FloorsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mNameTV;
        ImageView mPlanPhoto;

        FloorsViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.floor_fu_cv);
            mNameTV = (TextView) itemView.findViewById(R.id.floor_fu_label);
            mPlanPhoto = (ImageView) itemView.findViewById(R.id.floor_fu_image);
        }

    }

    private void openPlan(int floorId){
        Intent intent = new Intent(mContext, FloorPlanActivity.class);
        intent.putExtra("fId",floorId);
        mContext.startActivity(intent);
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }
        catch(Exception e){
            e.getMessage();
            return null;
        }
    }

}

