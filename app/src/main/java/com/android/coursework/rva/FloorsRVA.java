package com.android.coursework.rva;


import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.EditCompanyActivity;
import com.android.coursework.EditFloorActivity;
import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.model.Floor;
import com.android.coursework.R;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FloorsRVA extends RecyclerView.Adapter<FloorsRVA.FloorsViewHolder>{
    private Context mContext;
    private JSONPlaceHolderApi mApi;
    private FloorsViewHolder mFloorsViewHolder;

    public FloorsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.floor_card, viewGroup, false);

        FloorsViewHolder floorsViewHolder = new FloorsViewHolder(v);
        return floorsViewHolder;
    }
    private List<Floor> mFloors;
    public FloorsRVA(Context context, List<Floor> floors){
        this.mContext = context;
        this.mFloors = floors;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull FloorsViewHolder holder, int position) {
        holder.mCardView.setId(mFloors.get(position).getId());
        String label = mFloors.get(position).getNumber() + " " + mContext.getResources().getString(R.string.floor);
        holder.mNameTV.setText(label);
        mApi = NetworkService.getInstance().getApiService();
        holder.mPlanPhoto.setImageBitmap(StringToBitMap(mFloors.get(position).getPlanPhoto()));

        holder.mEditButton.setOnClickListener(v ->{
            editFloor(holder);
        });

        holder.mDeleteButton.setOnClickListener( v -> {
            mFloorsViewHolder = holder;
            deleteFloor(holder);
        });
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mFloors.size();
    }

    class FloorsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mNameTV;
        ImageView mPlanPhoto;
        Button mEditButton, mDeleteButton;

        FloorsViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.floor_cv);
            mNameTV = (TextView) itemView.findViewById(R.id.floor_label);
            mPlanPhoto = (ImageView) itemView.findViewById(R.id.floor_image);
            mEditButton = (Button) itemView.findViewById(R.id.edit_floor_btn);
            mDeleteButton = (Button) itemView.findViewById(R.id.del_floor_btn);
        }

    }

    private void deleteFloor(FloorsViewHolder holder) {
        mApi.deleteFloor(holder.mCardView.getId()).enqueue(deleteCallback);
    }

    Callback<Floor> deleteCallback = new Callback<Floor>() {
        @Override
        public void onResponse(Call<Floor> call, Response<Floor> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                mFloors.remove(mFloorsViewHolder.getAdapterPosition());
                notifyItemRemoved(mFloorsViewHolder.getAdapterPosition());
                notifyItemRangeChanged(mFloorsViewHolder.getAdapterPosition(), mFloors.size());
            }
        }

        @Override
        public void onFailure(Call<Floor> call, Throwable t) {
            System.out.println(t);
        }
    };
    private void editFloor(FloorsViewHolder holder) {
        Intent intent = new Intent(mContext, EditFloorActivity.class);
        intent.putExtra("fId", holder.mCardView.getId());
        mContext.startActivity(intent);
    }

    public Bitmap StringToBitMap(String encodedString){
        try{
            byte [] encodeByte = Base64.decode(encodedString,Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        }
        catch(Exception e){
            e.getMessage();
            return null;
        }
    }
}

