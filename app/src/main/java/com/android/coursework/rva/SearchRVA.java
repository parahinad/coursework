package com.android.coursework.rva;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.VisitedBuildingsActivity;
import com.android.coursework.model.Building;
import com.android.coursework.model.Company;
import com.android.coursework.R;

import java.util.List;

public class SearchRVA extends RecyclerView.Adapter<SearchRVA.BuildingsViewHolder>{

    private Context mContext;

    public BuildingsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_card, viewGroup, false);

        BuildingsViewHolder buildingsViewHolder = new BuildingsViewHolder(v);
        return buildingsViewHolder;
    }

    private List<Building> mBuildings;

    public SearchRVA(Context context, List<Building> buildings){
        this.mContext = context;
        this.mBuildings = buildings;
    }

    @Override
    public void onBindViewHolder(@NonNull BuildingsViewHolder holder, int position) {
        holder.mCardView.setId(mBuildings.get(position).getId());
        holder.mAddressTV.setText(mBuildings.get(position).getAddress());
        holder.mTypeTV.setText(mBuildings.get(position).getType());
        holder.mVisitedButton.setOnClickListener(v -> {
           addToVisited(holder.mCardView.getId());
        });
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mBuildings.size();
    }

    class BuildingsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mAddressTV, mTypeTV;
        Button mVisitedButton;

        BuildingsViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.search_cv);
            mAddressTV = (TextView) itemView.findViewById(R.id.address_text);
            mTypeTV = (TextView) itemView.findViewById(R.id.type_text);
            mVisitedButton = (Button) itemView.findViewById(R.id.add_visited_btn);
        }

    }

    private void addToVisited(int companyId){
        //TODO; add to db
        Intent intent = new Intent(mContext, VisitedBuildingsActivity.class);
        intent.putExtra("cId", companyId);
        mContext.startActivity(intent);
    }

}

