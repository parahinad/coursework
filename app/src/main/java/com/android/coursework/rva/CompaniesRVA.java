package com.android.coursework.rva;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.CompaniesActivity;
import com.android.coursework.EditBuildingActivity;
import com.android.coursework.EditCompanyActivity;
import com.android.coursework.NewCompanyActivity;
import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Company;
import com.android.coursework.R;
import com.android.coursework.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompaniesRVA extends RecyclerView.Adapter<CompaniesRVA.CompaniesViewHolder> {

    private Context mContext;
    private JSONPlaceHolderApi mApi;
    private CompaniesViewHolder mCompaniesViewHolder;

    public CompaniesViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.comp_card, viewGroup, false);
        mApi = NetworkService.getInstance().getApiService();

        CompaniesViewHolder companiesViewHolder = new CompaniesViewHolder(v);
        return companiesViewHolder;
    }

    private List<Company> mCompanies;

    public CompaniesRVA(Context context, List<Company> companies) {
        this.mContext = context;
        this.mCompanies = companies;
    }

    @Override
    public void onBindViewHolder(@NonNull CompaniesViewHolder holder, int position) {
        holder.mCardView.setId(mCompanies.get(position).getId());
        holder.mNameTV.setText(mCompanies.get(position).getName());
//        holder.mBuildNumTV.setText(mCompanies.get(position).getCountBuild());

        holder.mEditButton.setOnClickListener(v -> {
            editCompany(holder);
        });

        holder.mDeleteButton.setOnClickListener(v -> {
            mCompaniesViewHolder = holder;
            deleteCompany();
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mCompanies.size();
    }

    class CompaniesViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mNameTV, mBuildNumTV;
        Button mEditButton, mDeleteButton;

        CompaniesViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.comp_cv);
            mNameTV = (TextView) itemView.findViewById(R.id.comp_name);
            mBuildNumTV = (TextView) itemView.findViewById(R.id.build_num_tv);
            mEditButton = (Button) itemView.findViewById(R.id.edit_comp_btn);
            mDeleteButton = (Button) itemView.findViewById(R.id.del_comp_btn);
        }

    }

    private void deleteCompany() {
        mApi.deleteCompany(mCompaniesViewHolder.mCardView.getId()).enqueue(deleteCallback);
    }

    Callback<Company> deleteCallback = new Callback<Company>() {
        @Override
        public void onResponse(Call<Company> call, Response<Company> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                mCompanies.remove(mCompaniesViewHolder.getAdapterPosition());
                notifyItemRemoved(mCompaniesViewHolder.getAdapterPosition());
                notifyItemRangeChanged(mCompaniesViewHolder.getAdapterPosition(), mCompanies.size());
            }
        }

        @Override
        public void onFailure(Call<Company> call, Throwable t) {
            System.out.println(t);
        }
    };

    private void editCompany(CompaniesViewHolder holder) {
        Intent intent = new Intent(mContext, EditCompanyActivity.class);
        intent.putExtra("cId", holder.mCardView.getId());
        mContext.startActivity(intent);
    }

}

