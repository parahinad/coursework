package com.android.coursework.rva;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.EditBuildingActivity;
import com.android.coursework.EditCompanyActivity;
import com.android.coursework.FloorsActivity;
import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.R;
import com.android.coursework.model.Company;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuildingsRVA extends RecyclerView.Adapter<BuildingsRVA.BuildingsViewHolder>{
    private Context mContext;
    private JSONPlaceHolderApi mApi;
    private BuildingsViewHolder mBuildingsViewHolder;

    public BuildingsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.build_card, viewGroup, false);
        mApi = NetworkService.getInstance().getApiService();

        BuildingsViewHolder buildingsViewHolder = new BuildingsViewHolder(v);
        return buildingsViewHolder;
    }
    private List<Building> mBuildings;
    public BuildingsRVA(Context context, List<Building> building){
        this.mContext = context;
        this.mBuildings = building;
    }
    @Override
    public void onBindViewHolder(@NonNull BuildingsViewHolder holder, int position) {
        holder.mCardView.setId(mBuildings.get(position).getId());
        holder.mNameTV.setText(mBuildings.get(position).getType());
        holder.mAddressTV.setText(mBuildings.get(position).getAddress());


        holder.mEditButton.setOnClickListener(v -> {
            editBuilding(holder);
        });

        holder.mDeleteButton.setOnClickListener(v -> {
            mBuildingsViewHolder = holder;
            deleteBuilding();
        });
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mBuildings.size();
    }

    class BuildingsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mNameTV, mFloorsNumTV, mAddressTV;
        Button mEditButton, mDeleteButton;

        BuildingsViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.build_cv);
            mNameTV = (TextView) itemView.findViewById(R.id.build_name);
            mFloorsNumTV = (TextView) itemView.findViewById(R.id.floors_num);
            mAddressTV = (TextView) itemView.findViewById(R.id.address_text);
            mEditButton = (Button) itemView.findViewById(R.id.edit_build_btn);
            mDeleteButton = (Button) itemView.findViewById(R.id.del_build_btn);
        }

    }
    private void deleteBuilding() {
        mApi.deleteBuilding(mBuildingsViewHolder.mCardView.getId()).enqueue(deleteCallback);
    }
    Callback<Building> deleteCallback = new Callback<Building>() {
        @Override
        public void onResponse(Call<Building> call, Response<Building> response) {
            if (response.isSuccessful()) {
                System.out.println(response.body());
                mBuildings.remove(mBuildingsViewHolder.getAdapterPosition());
                notifyItemRemoved(mBuildingsViewHolder.getAdapterPosition());
                notifyItemRangeChanged(mBuildingsViewHolder.getAdapterPosition(), mBuildings.size());
            }
        }

        @Override
        public void onFailure(Call<Building> call, Throwable t) {
            System.out.println(t);
        }
    };

    private void editBuilding(BuildingsViewHolder holder) {
        Intent intent = new Intent(mContext, EditBuildingActivity.class);
        intent.putExtra("bId", holder.mCardView.getId());
        mContext.startActivity(intent);
    }

}

