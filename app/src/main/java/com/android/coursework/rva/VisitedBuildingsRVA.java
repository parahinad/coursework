package com.android.coursework.rva;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.coursework.FloorsActivity;
import com.android.coursework.FloorsForUsersActivity;
import com.android.coursework.R;
import com.android.coursework.model.Building;

import java.util.List;

public class VisitedBuildingsRVA extends RecyclerView.Adapter<VisitedBuildingsRVA.VisitedBuildingsViewHolder>{

    private Context mContext;
    private List<Building> mBuildings;

    public VisitedBuildingsRVA(Context context, List<Building> building){
        this.mContext = context;
        mBuildings = building;
    }

    public VisitedBuildingsRVA.VisitedBuildingsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.visited_build_card, viewGroup, false);

        VisitedBuildingsRVA.VisitedBuildingsViewHolder bvh
                = new VisitedBuildingsRVA.VisitedBuildingsViewHolder(v);
        return bvh;
    }

    @Override
    public void onBindViewHolder(@NonNull VisitedBuildingsRVA.VisitedBuildingsViewHolder holder, int position) {
        holder.mCardView.setId(mBuildings.get(position).getId());
        holder.mNameTV.setText(mBuildings.get(position).getType());
//        holder.mFloorsTV.setText(mBuildings.get(position).getCountFloor());
        holder.mAddressTV.setText(mBuildings.get(position).getAddress());

        holder.mCardView.setOnClickListener(v -> {
            goToFloors(holder.mCardView.getId());
        });

        holder.mDeleteBtn.setOnClickListener(v -> {
            deleteVisitedBuilding(holder);
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return mBuildings.size();
    }

    class VisitedBuildingsViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        TextView mNameTV, mFloorsTV, mAddressTV;
        Button mDeleteBtn;

        VisitedBuildingsViewHolder(View itemView) {
            super(itemView);

            mCardView = itemView.findViewById(R.id.visited_build_cv);
            mNameTV = itemView.findViewById(R.id.build_label);
            mAddressTV = itemView.findViewById(R.id.address_text);
            mFloorsTV = itemView.findViewById(R.id.floors_text);
            mDeleteBtn = itemView.findViewById(R.id.del_build_btn);
        }

    }

    private void deleteVisitedBuilding(VisitedBuildingsViewHolder holder) {
        //TODO; delete visited building
        mBuildings.remove(holder.getAdapterPosition());
        notifyItemRemoved(holder.getAdapterPosition());
        notifyItemRangeChanged(holder.getAdapterPosition(), mBuildings.size());
    }

    private void goToFloors(int buildingId) {
        Intent intent = new Intent(mContext, FloorsForUsersActivity.class);
        intent.putExtra("bId", buildingId);
        mContext.startActivity(intent);
    }
}