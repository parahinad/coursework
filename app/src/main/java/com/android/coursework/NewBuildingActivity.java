package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.Building;
import com.android.coursework.model.Company;
import com.android.coursework.model.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewBuildingActivity extends AppCompatActivity {

    private static final String TAG = "NewBuildingActivity";

    Button mCancelButton, mSaveButton;
    EditText mBuildingAddress, mBuildingType;

    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_building);

        mApi = NetworkService.getInstance().getApiService();

        mCancelButton = findViewById(R.id.cancel_add_build_btn);
        mSaveButton = findViewById(R.id.save_add_build_btn);
        mBuildingAddress = findViewById(R.id.add_address_input);
        mBuildingType = findViewById(R.id.add_type_input);

        mCancelButton.setOnClickListener(v -> {
            finish();
        });

        mSaveButton.setOnClickListener(v -> {
            saveBuilding();
        });
    }

    private void saveBuilding(){
        Building building = new Building();
        building.setCompanyId(getIntent().getIntExtra("cId", -1));
        building.setType(mBuildingType.getText().toString());
        building.setAddress(mBuildingAddress.getText().toString());
        mApi.createBuilding(building).enqueue(createBuildingCallback);
    }

    Callback<Building> createBuildingCallback = new Callback<Building>() {
        @Override
        public void onResponse(Call<Building> call, Response<Building> response) {
            if(response.isSuccessful()) {
                finish();
            }
        }

        @Override
        public void onFailure(Call<Building> call, Throwable t) {
            Log.i(TAG, t.getMessage());
        }
    };
}