package com.android.coursework;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.coursework.api.JSONPlaceHolderApi;
import com.android.coursework.api.NetworkService;
import com.android.coursework.model.User;
import com.android.coursework.utils.Verification;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private EditText mEmailEditText;
    private EditText mPasswordEditText;
    private Button mLoginButton;
    private TextView mGoToRegistration;
    private JSONPlaceHolderApi mApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEmailEditText = findViewById(R.id.log_email_input);
        mPasswordEditText = findViewById(R.id.log_pass_input);
        mLoginButton = findViewById(R.id.login_btn);
        mLoginButton.setOnClickListener((v) -> {
            if (Verification.verifyEmail(this, mEmailEditText) &
                    Verification.verifyPassword(this, mPasswordEditText)) {
                loginUser();
            }
        });

        mGoToRegistration = findViewById(R.id.log_reg_btn);
        mGoToRegistration.setOnClickListener((v) -> navigateToRegisterScreen());

        mApi = NetworkService.getInstance().getApiService();
    }

    private void loginUser() {
        User user = User
                .getInstance()
                .setPassword(mPasswordEditText.getText().toString())
                .setEmail(mEmailEditText.getText().toString());

        mApi.loginUser(user).enqueue(loginCallback);
    }

    private Callback<String> loginCallback = new Callback<String>() {
        @Override
        public void onResponse(Call<String> call, Response<String> response) {
            List<String> CookieList = response.headers().values("Set-Cookie");
            if (CookieList.size() != 0) {
                getUserData(User.getInstance().getEmail());
            } else {
                Toast.makeText(LoginActivity.this, getResources().getText(R.string.wrong_credentials), Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onFailure(Call<String> call, Throwable t) {
            System.out.println(t.getMessage());
        }

    };

    private void getUserData(String sessionId) {
        mApi.getUserData(sessionId).enqueue(dataCallback);
    }

    private Callback<User> dataCallback = new Callback<User>() {
        @Override
        public void onResponse(Call<User> call, Response<User> response) {
            if(response.body().getRole() == null) {
                User.getInstance().setId(response.body().getId()).setRole("");
            }else {
                User.getInstance().setId(response.body().getId()).setRole(response.body().getRole());
            }
                navigateToMenuScreen();
        }

        @Override
        public void onFailure(Call<User> call, Throwable t) {
            System.out.println(t.getMessage());
        }
    };

    private void navigateToMenuScreen() {
        Intent intent = new Intent(LoginActivity.this,
                MenuActivity.class);
        startActivity(intent);
    }

    private void navigateToRegisterScreen() {
        Intent intent = new Intent(LoginActivity.this,
                RegisterActivity.class);
        startActivity(intent);
    }
}